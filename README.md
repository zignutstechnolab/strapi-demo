# Strapi application

1. Clone the repository.

2. Install dependencies using **npm install** command.

3. Create `.env` file on root folder. then copy `.env.example` file to `.env` file. then update database and email credentials.

4. Find database file (.sql file) inside `DB` folder and postman collection (.json file) inside `postman_collection` folder.

5. Run application using **npm run develop** for development mode. (default port is 1337)

6. Admin credential : username => **admin**, password => **admin@123**
