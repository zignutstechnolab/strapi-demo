"use strict";

var knex = require("../../../config/knex");
/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  /**
   * @name updatePrice
   * @file activity.js
   * @param {Request} discount
   * @param {Response} res
   * @description This method is used to update price of all activities by discount percentage
   */
  updatePrice: async (ctx) => {
    const { discount } = ctx.request.body;

    if (!discount || discount > 100 || discount < 0)
      return ctx.send({
        statusCode: 400,
        error: "Please enter valid discount",
      });

    await knex.schema
      .raw("update activities set price = price - (price * ? / 100.0)", [
        discount,
      ])
      .then(
        (result) => {
          //console.log(result);
          ctx.send({
            statusCode: 200,
            message: "Price updated successfully!",
            data: result,
          });
        },
        (error) => {
          //console.log(error);
          return ctx.send({
            statusCode: 400,
            error: error,
          });
        }
      );
  },
};
