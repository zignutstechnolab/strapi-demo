"use strict";
const nodemailer = require("nodemailer");

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/models.html#life-cycle-callbacks)
 * to customize this model
 */

module.exports = {
  lifecycles: {
    async afterCreate(data) {
      console.log("Activity created");
      let transport = nodemailer.createTransport({
        host: process.env.MAIL_HOST,
        port: process.env.MAIL_PORT,
        auth: {
          user: process.env.MAIL_USERNAME,
          pass: process.env.MAIL_PASSWORD,
        },
        secure: process.env.MAIL_SECURE == "false" ? false : true,
      });
      const message = {
        from: process.env.MAIL_FROM_ADDRESS,
        to: process.env.MAIL_TO_ADDRESS,
        subject: "New Activity Created!",
        html: `<p>New activity named <b>${data.title}</b> is created by an admin.</p>`,
      };
      transport.sendMail(message, function (err, info) {
        if (err) {
          console.log(err);
        } else {
          console.log(info);
        }
      });
    },
  },
};
