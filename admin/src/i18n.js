import translationMessages from "./translations";

const languages = Object.keys(translationMessages);

export { languages, translationMessages };
