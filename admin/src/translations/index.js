import en from "./en.json";
import de from "./de.json";
import es from "./es.json";

const trads = {
  en,
  de,
  es,
};

export default trads;
